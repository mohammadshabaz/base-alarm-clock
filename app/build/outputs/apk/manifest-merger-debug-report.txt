-- Merging decision tree log ---
manifest
ADDED from AndroidManifest.xml:1:1
	package
		ADDED from AndroidManifest.xml:2:9
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
	android:versionName
		ADDED from AndroidManifest.xml:3:35
	android:versionCode
		ADDED from AndroidManifest.xml:3:9
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
	xmlns:android
		ADDED from AndroidManifest.xml:1:11
original-package
ADDED from AndroidManifest.xml:5:5
	android:name
		ADDED from AndroidManifest.xml:5:23
uses-permission#android.permission.RECEIVE_BOOT_COMPLETED
ADDED from AndroidManifest.xml:8:5
	android:name
		ADDED from AndroidManifest.xml:8:22
uses-permission#android.permission.WAKE_LOCK
ADDED from AndroidManifest.xml:9:5
	android:name
		ADDED from AndroidManifest.xml:9:22
uses-permission#android.permission.VIBRATE
ADDED from AndroidManifest.xml:10:5
	android:name
		ADDED from AndroidManifest.xml:10:22
uses-permission#android.permission.WRITE_SETTINGS
ADDED from AndroidManifest.xml:11:5
	android:name
		ADDED from AndroidManifest.xml:11:22
uses-permission#android.permission.DISABLE_KEYGUARD
ADDED from AndroidManifest.xml:12:5
	android:name
		ADDED from AndroidManifest.xml:12:22
uses-permission#android.permission.READ_PHONE_STATE
ADDED from AndroidManifest.xml:13:5
	android:name
		ADDED from AndroidManifest.xml:13:22
uses-permission#android.permission.READ_EXTERNAL_STORAGE
ADDED from AndroidManifest.xml:14:5
	android:name
		ADDED from AndroidManifest.xml:14:22
uses-sdk
ADDED from AndroidManifest.xml:15:5
MERGED from com.android.support:support-v4:22.2.0:20:5
MERGED from com.android.support:support-v13:20.0.0:16:5
MERGED from com.android.support:support-v4:22.2.0:20:5
	android:targetSdkVersion
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
	android:minSdkVersion
		ADDED from AndroidManifest.xml:15:15
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
application
ADDED from AndroidManifest.xml:17:5
MERGED from com.android.support:support-v4:22.2.0:22:5
MERGED from com.android.support:support-v13:20.0.0:17:5
MERGED from com.android.support:support-v4:22.2.0:22:5
	android:label
		ADDED from AndroidManifest.xml:17:18
	android:icon
		ADDED from AndroidManifest.xml:18:18
provider#com.bazzingalabs.deskclock.AlarmProvider
ADDED from AndroidManifest.xml:20:9
	android:authorities
		ADDED from AndroidManifest.xml:21:17
	android:exported
		ADDED from AndroidManifest.xml:22:17
	android:name
		ADDED from AndroidManifest.xml:20:19
activity#com.bazzingalabs.deskclock.DeskClock
ADDED from AndroidManifest.xml:24:9
	android:label
		ADDED from AndroidManifest.xml:25:17
	android:launchMode
		ADDED from AndroidManifest.xml:28:17
	android:icon
		ADDED from AndroidManifest.xml:27:17
	android:theme
		ADDED from AndroidManifest.xml:26:17
	android:name
		ADDED from AndroidManifest.xml:24:19
intent-filter#android.intent.action.MAIN+android.intent.category.DEFAULT+android.intent.category.LAUNCHER
ADDED from AndroidManifest.xml:31:13
action#android.intent.action.MAIN
ADDED from AndroidManifest.xml:32:17
	android:name
		ADDED from AndroidManifest.xml:32:25
category#android.intent.category.DEFAULT
ADDED from AndroidManifest.xml:33:17
	android:name
		ADDED from AndroidManifest.xml:33:27
category#android.intent.category.LAUNCHER
ADDED from AndroidManifest.xml:34:17
	android:name
		ADDED from AndroidManifest.xml:34:27
activity-alias#com.bazzingalabs.deskclock.DockClock
ADDED from AndroidManifest.xml:38:9
	android:enabled
		ADDED from AndroidManifest.xml:44:17
	android:label
		ADDED from AndroidManifest.xml:40:17
	android:launchMode
		ADDED from AndroidManifest.xml:43:17
	android:targetActivity
		ADDED from AndroidManifest.xml:39:17
	android:icon
		ADDED from AndroidManifest.xml:42:17
	android:theme
		ADDED from AndroidManifest.xml:41:17
	android:name
		ADDED from AndroidManifest.xml:38:25
intent-filter#android.intent.action.MAIN+android.intent.category.DEFAULT+android.intent.category.DESK_DOCK
ADDED from AndroidManifest.xml:46:13
category#android.intent.category.DESK_DOCK
ADDED from AndroidManifest.xml:49:17
	android:name
		ADDED from AndroidManifest.xml:49:27
activity#com.bazzingalabs.deskclock.AlarmClock
ADDED from AndroidManifest.xml:53:9
	android:label
		ADDED from AndroidManifest.xml:54:17
	android:excludeFromRecents
		ADDED from AndroidManifest.xml:57:17
	android:launchMode
		ADDED from AndroidManifest.xml:58:17
	android:exported
		ADDED from AndroidManifest.xml:59:17
	android:theme
		ADDED from AndroidManifest.xml:55:17
	android:taskAffinity
		ADDED from AndroidManifest.xml:56:17
	android:name
		ADDED from AndroidManifest.xml:53:19
activity-alias#com.bazzingalabs.alarmclock.AlarmClock
ADDED from AndroidManifest.xml:61:9
	android:exported
		ADDED from AndroidManifest.xml:63:17
	android:targetActivity
		ADDED from AndroidManifest.xml:62:17
	android:name
		ADDED from AndroidManifest.xml:61:25
activity#com.bazzingalabs.deskclock.SettingsActivity
ADDED from AndroidManifest.xml:65:9
	android:label
		ADDED from AndroidManifest.xml:66:17
	android:excludeFromRecents
		ADDED from AndroidManifest.xml:69:17
	android:theme
		ADDED from AndroidManifest.xml:67:17
	android:taskAffinity
		ADDED from AndroidManifest.xml:68:17
	android:name
		ADDED from AndroidManifest.xml:65:19
intent-filter#android.intent.action.MAIN
ADDED from AndroidManifest.xml:71:13
activity#com.bazzingalabs.deskclock.worldclock.CitiesActivity
ADDED from AndroidManifest.xml:76:9
	android:label
		ADDED from AndroidManifest.xml:77:17
	android:excludeFromRecents
		ADDED from AndroidManifest.xml:80:17
	android:theme
		ADDED from AndroidManifest.xml:78:17
	android:taskAffinity
		ADDED from AndroidManifest.xml:79:17
	android:name
		ADDED from AndroidManifest.xml:76:19
activity#com.bazzingalabs.deskclock.SetAlarm
ADDED from AndroidManifest.xml:87:9
	android:label
		ADDED from AndroidManifest.xml:88:17
	android:theme
		ADDED from AndroidManifest.xml:89:17
	android:name
		ADDED from AndroidManifest.xml:87:19
activity#com.bazzingalabs.deskclock.AlarmAlert
ADDED from AndroidManifest.xml:91:9
	android:excludeFromRecents
		ADDED from AndroidManifest.xml:92:17
	android:launchMode
		ADDED from AndroidManifest.xml:94:17
	android:configChanges
		ADDED from AndroidManifest.xml:96:17
	android:theme
		ADDED from AndroidManifest.xml:93:17
	android:taskAffinity
		ADDED from AndroidManifest.xml:95:17
	android:name
		ADDED from AndroidManifest.xml:91:19
activity#com.bazzingalabs.deskclock.AlarmAlertFullScreen
ADDED from AndroidManifest.xml:100:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:106:17
	android:excludeFromRecents
		ADDED from AndroidManifest.xml:101:17
	android:launchMode
		ADDED from AndroidManifest.xml:103:17
	android:configChanges
		ADDED from AndroidManifest.xml:107:17
	android:showOnLockScreen
		ADDED from AndroidManifest.xml:105:17
	android:theme
		ADDED from AndroidManifest.xml:102:17
	android:taskAffinity
		ADDED from AndroidManifest.xml:104:17
	android:name
		ADDED from AndroidManifest.xml:100:19
activity#com.bazzingalabs.deskclock.ScreensaverActivity
ADDED from AndroidManifest.xml:109:9
	android:excludeFromRecents
		ADDED from AndroidManifest.xml:110:17
	android:configChanges
		ADDED from AndroidManifest.xml:113:17
	android:theme
		ADDED from AndroidManifest.xml:112:17
	android:taskAffinity
		ADDED from AndroidManifest.xml:111:17
	android:name
		ADDED from AndroidManifest.xml:109:19
receiver#com.bazzingalabs.deskclock.AlarmReceiver
ADDED from AndroidManifest.xml:115:9
	android:exported
		ADDED from AndroidManifest.xml:116:17
	android:name
		ADDED from AndroidManifest.xml:115:19
intent-filter#alarm_killed+cancel_snooze+com.bazzingalabs.deskclock.ALARM_ALERT
ADDED from AndroidManifest.xml:117:13
action#com.bazzingalabs.deskclock.ALARM_ALERT
ADDED from AndroidManifest.xml:118:17
	android:name
		ADDED from AndroidManifest.xml:118:25
action#alarm_killed
ADDED from AndroidManifest.xml:119:17
	android:name
		ADDED from AndroidManifest.xml:119:25
action#cancel_snooze
ADDED from AndroidManifest.xml:120:17
	android:name
		ADDED from AndroidManifest.xml:120:25
activity#com.bazzingalabs.deskclock.HandleSetAlarm
ADDED from AndroidManifest.xml:124:9
	android:excludeFromRecents
		ADDED from AndroidManifest.xml:126:17
	android:permission
		ADDED from AndroidManifest.xml:127:17
	android:theme
		ADDED from AndroidManifest.xml:125:17
	android:name
		ADDED from AndroidManifest.xml:124:19
intent-filter#android.intent.action.SET_ALARM+android.intent.category.DEFAULT
ADDED from AndroidManifest.xml:128:13
action#android.intent.action.SET_ALARM
ADDED from AndroidManifest.xml:129:17
	android:name
		ADDED from AndroidManifest.xml:129:25
service#com.bazzingalabs.deskclock.AlarmKlaxon
ADDED from AndroidManifest.xml:140:9
	android:exported
		ADDED from AndroidManifest.xml:141:17
	android:description
		ADDED from AndroidManifest.xml:142:17
	android:name
		ADDED from AndroidManifest.xml:140:18
intent-filter#com.bazzingalabs.deskclock.ALARM_ALERT
ADDED from AndroidManifest.xml:143:13
receiver#com.bazzingalabs.deskclock.AlarmInitReceiver
ADDED from AndroidManifest.xml:148:9
	android:name
		ADDED from AndroidManifest.xml:148:19
intent-filter#android.intent.action.BOOT_COMPLETED+android.intent.action.LOCALE_CHANGED+android.intent.action.TIMEZONE_CHANGED+android.intent.action.TIME_SET
ADDED from AndroidManifest.xml:149:13
action#android.intent.action.BOOT_COMPLETED
ADDED from AndroidManifest.xml:150:17
	android:name
		ADDED from AndroidManifest.xml:150:25
action#android.intent.action.TIME_SET
ADDED from AndroidManifest.xml:151:17
	android:name
		ADDED from AndroidManifest.xml:151:25
action#android.intent.action.TIMEZONE_CHANGED
ADDED from AndroidManifest.xml:152:17
	android:name
		ADDED from AndroidManifest.xml:152:25
action#android.intent.action.LOCALE_CHANGED
ADDED from AndroidManifest.xml:153:17
	android:name
		ADDED from AndroidManifest.xml:153:25
receiver#com.bazzingalabs.alarmclock.AnalogAppWidgetProvider
ADDED from AndroidManifest.xml:157:9
	android:label
		ADDED from AndroidManifest.xml:157:86
	android:icon
		ADDED from AndroidManifest.xml:158:12
	android:name
		ADDED from AndroidManifest.xml:157:19
intent-filter#android.appwidget.action.APPWIDGET_UPDATE
ADDED from AndroidManifest.xml:159:13
action#android.appwidget.action.APPWIDGET_UPDATE
ADDED from AndroidManifest.xml:160:17
	android:name
		ADDED from AndroidManifest.xml:160:25
meta-data#android.appwidget.oldName
ADDED from AndroidManifest.xml:162:13
	android:value
		ADDED from AndroidManifest.xml:162:65
	android:name
		ADDED from AndroidManifest.xml:162:24
meta-data#android.appwidget.provider
ADDED from AndroidManifest.xml:163:13
	android:resource
		ADDED from AndroidManifest.xml:163:66
	android:name
		ADDED from AndroidManifest.xml:163:24
receiver#com.bazzingalabs.alarmclock.DigitalAppWidgetProvider
ADDED from AndroidManifest.xml:166:9
	android:label
		ADDED from AndroidManifest.xml:166:87
	android:icon
		ADDED from AndroidManifest.xml:167:12
	android:name
		ADDED from AndroidManifest.xml:166:19
service#com.bazzingalabs.alarmclock.DigitalAppWidgetService
ADDED from AndroidManifest.xml:174:9
	android:exported
		ADDED from AndroidManifest.xml:176:14
	android:permission
		ADDED from AndroidManifest.xml:175:14
	android:name
		ADDED from AndroidManifest.xml:174:18
receiver#com.bazzingalabs.alarmclock.DigitalWidgetViewsFactory
ADDED from AndroidManifest.xml:178:9
	android:exported
		ADDED from AndroidManifest.xml:179:14
	android:name
		ADDED from AndroidManifest.xml:178:19
service#com.bazzingalabs.deskclock.Screensaver
ADDED from AndroidManifest.xml:182:9
	android:label
		ADDED from AndroidManifest.xml:184:13
	android:exported
		ADDED from AndroidManifest.xml:183:13
	android:name
		ADDED from AndroidManifest.xml:182:18
intent-filter#android.intent.category.DEFAULT+android.service.dreams.DreamService
ADDED from AndroidManifest.xml:185:13
action#android.service.dreams.DreamService
ADDED from AndroidManifest.xml:186:17
	android:name
		ADDED from AndroidManifest.xml:186:25
meta-data#android.service.dream
ADDED from AndroidManifest.xml:189:13
	android:resource
		ADDED from AndroidManifest.xml:191:17
	android:name
		ADDED from AndroidManifest.xml:190:17
activity#com.bazzingalabs.deskclock.ScreensaverSettingsActivity
ADDED from AndroidManifest.xml:195:9
	android:label
		ADDED from AndroidManifest.xml:196:17
	android:excludeFromRecents
		ADDED from AndroidManifest.xml:199:17
	android:exported
		ADDED from AndroidManifest.xml:200:17
	android:theme
		ADDED from AndroidManifest.xml:197:17
	android:taskAffinity
		ADDED from AndroidManifest.xml:198:17
	android:name
		ADDED from AndroidManifest.xml:195:19
activity#com.bazzingalabs.deskclock.timer.TimerAlertFullScreen
ADDED from AndroidManifest.xml:210:9
	android:excludeFromRecents
		ADDED from AndroidManifest.xml:211:17
	android:launchMode
		ADDED from AndroidManifest.xml:213:17
	android:configChanges
		ADDED from AndroidManifest.xml:216:17
	android:showOnLockScreen
		ADDED from AndroidManifest.xml:214:17
	android:theme
		ADDED from AndroidManifest.xml:212:17
	android:taskAffinity
		ADDED from AndroidManifest.xml:215:17
	android:name
		ADDED from AndroidManifest.xml:210:19
service#com.bazzingalabs.deskclock.TimerRingService
ADDED from AndroidManifest.xml:218:9
	android:exported
		ADDED from AndroidManifest.xml:219:17
	android:description
		ADDED from AndroidManifest.xml:220:17
	android:name
		ADDED from AndroidManifest.xml:218:18
intent-filter#com.bazzingalabs.deskclock.TIMER_ALERT
ADDED from AndroidManifest.xml:221:13
action#com.bazzingalabs.deskclock.TIMER_ALERT
ADDED from AndroidManifest.xml:222:17
	android:name
		ADDED from AndroidManifest.xml:222:25
receiver#com.bazzingalabs.deskclock.timer.TimerReceiver
ADDED from AndroidManifest.xml:226:9
	android:exported
		ADDED from AndroidManifest.xml:227:17
	android:name
		ADDED from AndroidManifest.xml:226:19
intent-filter#delete_timer+notif_in_use_cancel+notif_in_use_show+start_timer+timer_done+timer_reset+timer_stop+timer_update+times_up
ADDED from AndroidManifest.xml:228:13
action#start_timer
ADDED from AndroidManifest.xml:229:17
	android:name
		ADDED from AndroidManifest.xml:229:25
action#delete_timer
ADDED from AndroidManifest.xml:230:17
	android:name
		ADDED from AndroidManifest.xml:230:25
action#times_up
ADDED from AndroidManifest.xml:231:17
	android:name
		ADDED from AndroidManifest.xml:231:25
action#timer_stop
ADDED from AndroidManifest.xml:232:17
	android:name
		ADDED from AndroidManifest.xml:232:25
action#timer_reset
ADDED from AndroidManifest.xml:233:17
	android:name
		ADDED from AndroidManifest.xml:233:25
action#timer_done
ADDED from AndroidManifest.xml:234:17
	android:name
		ADDED from AndroidManifest.xml:234:25
action#timer_update
ADDED from AndroidManifest.xml:235:17
	android:name
		ADDED from AndroidManifest.xml:235:25
action#notif_in_use_show
ADDED from AndroidManifest.xml:236:17
	android:name
		ADDED from AndroidManifest.xml:236:25
action#notif_in_use_cancel
ADDED from AndroidManifest.xml:237:17
	android:name
		ADDED from AndroidManifest.xml:237:25
service#com.bazzingalabs.deskclock.stopwatch.StopwatchService
ADDED from AndroidManifest.xml:241:9
	android:exported
		ADDED from AndroidManifest.xml:242:17
	android:description
		ADDED from AndroidManifest.xml:243:17
	android:name
		ADDED from AndroidManifest.xml:241:18
intent-filter#lap_stopwatch+reset_stopwatch+share_stopwatch+start_stopwatch+stop_stopwatch
ADDED from AndroidManifest.xml:244:13
action#start_stopwatch
ADDED from AndroidManifest.xml:245:17
	android:name
		ADDED from AndroidManifest.xml:245:25
action#lap_stopwatch
ADDED from AndroidManifest.xml:246:17
	android:name
		ADDED from AndroidManifest.xml:246:25
action#stop_stopwatch
ADDED from AndroidManifest.xml:247:17
	android:name
		ADDED from AndroidManifest.xml:247:25
action#reset_stopwatch
ADDED from AndroidManifest.xml:248:17
	android:name
		ADDED from AndroidManifest.xml:248:25
action#share_stopwatch
ADDED from AndroidManifest.xml:249:17
	android:name
		ADDED from AndroidManifest.xml:249:25
